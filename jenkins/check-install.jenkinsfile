def dockerTestingEnv = "dockerInstallTest"
def sourceTestingEnv = "sourceInstallTest"
def testingGroup = "testingInstall"

def failCount = 0
def retryCount = 3
def slackResponse = null

//////////////////////////////////////////
// Pipeline
pipeline
{
    agent none
    options {
        ansiColor('xterm')
        retry(retryCount)
    }
    environment
    {
        NexusDockerRegistryUrl = "${env.NEXUS_REGISTRY_IP}:${env.NEXUS_REGISTRY_PORT}"
        DOCKER_INSTALL_SCRIPTS_URL = "https://neurorobotics.net/downloads/nrp_installer.sh"
        SOURCE_INSTALL_SCRIPTS_URL = "https://neurorobotics.net/downloads/source_install.bash"
    }

    stages
    {
        stage('Create VM for test')
        {
            agent {
                dockerfile {
                    label 'cd_label'
                    filename 'ubuntu-openstack-castor.Dockerfile'
                    dir 'openstack/castor'
                }
            }
            steps 
            {
                // bp00sp10 is a CSCS project code for the NRP
                withCredentials([ file(credentialsId: 'cscs-clouds-yaml', variable: 'CLOUDS_YAML') ]) {
                    sh '''
                        cd openstack/castor/scripts
                        export OS_CLOUD=openstack; cp $CLOUDS_YAML /etc/openstack/clouds.yaml
                        python3 env.up.py --env ''' + dockerTestingEnv + ''' --purpose ''' + testingGroup + ''' --nfrontend 1 --nbackend 1 --backend-flavor "m1.x-large" --frontend-flavor "m1.x-large" --force 
                        python3 env.up.py --env ''' + sourceTestingEnv + ''' --purpose ''' + testingGroup + ''' --nfrontend 1 --nbackend 1 --backend-flavor "m1.x-large" --frontend-flavor "m1.x-large" --force 
                        python3 updateInventory.py
                        # The VM is not that fast to boot, we sleep to allow the nex stage to SSH
                        sleep 30
                    '''
                }
                sh 'cp -f openstack/castor/scripts/inventory.json ansible/inventories/nrp/hosts.json'
                stash includes: 'ansible/inventories/nrp/hosts.json', name: 'inventory'
            }
        }

        stage('Run install script')
        {
            parallel 
            {    
                stage('Docker latest')
                {
                    agent { label 'cd_label' }
                    steps
                    {
                        unstash 'inventory' 
                        script 
                        {
                            sh 'rm -rf ansible/logs'
                            ansiblePlaybook(credentialsId: "${env.CASTOR_DEPLOY_KEY}", \
                                            colorized: true, \
                                            inventory: 'ansible/inventories/nrp', \
                                            playbook: 'ansible/nrp_check_install_script.yml', \
                                            limit : "${dockerTestingEnv}_backends", \
                                            extraVars: [ \
                                                installer_url : "${env.DOCKER_INSTALL_SCRIPTS_URL}", \
                                                script_args : "install latest -s"
                                            ] )
                        }
                        archiveArtifacts artifacts: 'ansible/logs/**/*.log', allowEmptyArchive: false
                    }
                } 
                stage('Docker legacy')
                {
                    agent { label 'cd_label' }
                    steps
                    {
                        unstash 'inventory' 
                        script 
                        {
                            sh 'rm -rf ansible/logs'
                            ansiblePlaybook(credentialsId: "${env.CASTOR_DEPLOY_KEY}", \
                                            colorized: true, \
                                            inventory: 'ansible/inventories/nrp', \
                                            playbook: 'ansible/nrp_check_install_script.yml', \
                                            limit : "${dockerTestingEnv}_frontends", \
                                            extraVars: [ \
                                                installer_url : "${env.DOCKER_INSTALL_SCRIPTS_URL}", \
                                                script_args : "install legacy -s"
                                            ] )
                        }
                        archiveArtifacts artifacts: 'ansible/logs/**/*.log', allowEmptyArchive: false
                    }
                } 
                stage('Source master')
                {
                    agent { label 'cd_label' }
                    steps
                    {
                        unstash 'inventory' 
                        script 
                        {
                            sh 'rm -rf ansible/logs'
                            ansiblePlaybook(credentialsId: "${env.CASTOR_DEPLOY_KEY}", \
                                            colorized: true, \
                                            inventory: 'ansible/inventories/nrp', \
                                            playbook: 'ansible/nrp_check_install_script.yml', \
                                            limit : "${sourceTestingEnv}_backends", \
                                            extraVars: [ \
                                                installer_url : "${env.SOURCE_INSTALL_SCRIPTS_URL}", \
                                                script_args : "master"
                                            ] )
                        }
                        archiveArtifacts artifacts: 'ansible/logs/**/*.log', allowEmptyArchive: false
                    }
                }
                stage('Source development')
                {
                    agent { label 'cd_label' }
                    steps
                    {
                        unstash 'inventory' 
                        script 
                        {
                            sh 'rm -rf ansible/logs'
                            ansiblePlaybook(credentialsId: "${env.CASTOR_DEPLOY_KEY}", \
                                            colorized: true, \
                                            inventory: 'ansible/inventories/nrp', \
                                            playbook: 'ansible/nrp_check_install_script.yml', \
                                            limit : "${sourceTestingEnv}_frontends", \
                                            extraVars: [ \
                                                installer_url : "${env.SOURCE_INSTALL_SCRIPTS_URL}", \
                                                script_args : "development"
                                            ] )
                        }
                        archiveArtifacts artifacts: 'ansible/logs/**/*.log', allowEmptyArchive: false
                    }
                }
            }
        }
        stage('Test docker installation')
        {
            agent { label 'cd_label' }
            steps
            {
                unstash 'inventory' 
                script 
                {
                    ansiblePlaybook(credentialsId: "${env.CASTOR_DEPLOY_KEY}", \
                                    colorized: true, \
                                    inventory: 'ansible/inventories/nrp', \
                                    playbook: 'ansible/nrp_test_local_install.yml', \
                                    limit : "${testingGroup}", \
                                    forks : 20, \
                                    extras: '-vvv')
                }
            }
        }
    }
    post{
        failure{
            script{
                failCount = failCount + 1
                if (slackResponse != null){
                    slackSend(channel: slackResponse.threadId, color: "danger", message: "Source installation job failed (attempt ${failCount.toString()}/${retryCount.toString()}).\n${BUILD_URL}", timestamp: slackResponse.ts)
                }
                else {      
                    slackResponse = slackSend(color: "warning", message: "Source installation job failed (attempt ${failCount.toString()}/${retryCount.toString()}).\n${BUILD_URL}")              
                }
            }
        }
        success{
            script{
                if (slackResponse != null){
                    slackSend(channel: slackResponse.threadId, color: "good", message: "Source installation job succeeded (failed attempts ${failCount.toString()}).\n${BUILD_URL}", timestamp: slackResponse.ts)
                }
            }
        }
    }
}