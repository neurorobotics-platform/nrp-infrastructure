#!/bin/bash

TARGET_ENV="development"
IMAGE_TAG="xpra"
BRANCH="development"
BASE_BRANCH="development"
DOCKER_REGISTRY=nexus.neurorobotics.ebrains.eu

# ANSIBLE_SSH_KEY=
# OIDC_CLIENT=
# OIDC_SECRET=
# DOCKER_PASS=
# DOCKER_USER=

if [ -z $ANSIBLE_SSH_KEY ]; then
    echo Define ANSIBLE_SSH_KEY env variable
    exit 1
fi

if [ -z $OIDC_CLIENT ]; then
    echo Define OIDC_CLIENT env variable
    exit 1
fi

if [ -z $OIDC_SECRET ]; then
    echo Define OIDC_SECRET env variable
    exit 1
fi

if [ -z $DOCKER_PASS ]; then
    echo Define DOCKER_PASS env variable
    exit 1
fi

if [ -z $DOCKER_USER ]; then
    echo Define DOCKER_USER env variable
    exit 1
fi

# We need to switch to the repository root directory.
cd $(dirname "$0")/..

export ANSIBLE_HOST_KEY_CHECKING=False

# update VMs
ansible-playbook \
    -l "${TARGET_ENV}" \
    -i ansible/inventories/nrp4 ansible/nrp_update.yml \
    --key-file "${ANSIBLE_SSH_KEY}" \
    --forks 10 \
    -e ssh_key_path="${ANSIBLE_SSH_KEY}" \
    -e docker_tag="${IMAGE_TAG}" \
    -e branch_name="${BRANCH}" \
    -e base_name="${BASE_BRANCH}" \
    -e docker_reg="${DOCKER_REGISTRY}" \
    -e docker_user="${DOCKER_USER}" \
    -e docker_pass="${DOCKER_PASS}"

# install nrp
ansible-playbook \
    -l "${TARGET_ENV}" \
    -i ansible/inventories/nrp4 ansible/nrp4_install.yml \
    --key-file "${ANSIBLE_SSH_KEY}" \
    -e ssh_key_path="${ANSIBLE_SSH_KEY}" \
    --forks 10 \
    -e docker_reg="${DOCKER_REGISTRY}" \
    -e docker_tag="${IMAGE_TAG}" \
    -e branch_name="${BRANCH}" \
    -e base_name="${BASE_BRANCH}"

# run nrp
ansible-playbook \
    -l "${TARGET_ENV}" \
    -i ansible/inventories/nrp4 ansible/nrp4_run.yml \
    --key-file "${ANSIBLE_SSH_KEY}" \
    -e ssh_key_path="${ANSIBLE_SSH_KEY}" \
    --forks 10 \
    -e docker_reg="${DOCKER_REGISTRY}" \
    -e docker_tag="${IMAGE_TAG}" \
    -e branch_name="${BRANCH}" \
    -e base_name="${BASE_BRANCH}" \
    -e oidc_proxy_client_id="${OIDC_CLIENT}" \
    -e oidc_proxy_secret="${OIDC_SECRET}"
