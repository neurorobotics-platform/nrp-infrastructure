def call(checkoutDir="code"){

    // Before starting pipeline, we try to get the proper image tag
    def DEFAULT_BRANCH = 'development'
    def TOPIC_BRANCH = selectTopicBranch(env.BRANCH_NAME, env.CHANGE_BRANCH)
    // We try to pull the image with the topic name, or use default tag otherwise
    def IMG_TAG = checkImageTag("${TOPIC_BRANCH}", "${DEFAULT_BRANCH}")

    pipeline {
        environment {
            USER_SCRIPTS_DIR = "user-scripts"
            // GIT_CHECKOUT_DIR is a dir of the main project (that was pushed)
            GIT_CHECKOUT_DIR = "${checkoutDir}"

            // That is needed to pass the variables into environment with the same name from 
            // Jenkins global scope (def ..=..)
            TOPIC_BRANCH = "${TOPIC_BRANCH}"
            DEFAULT_BRANCH = "${DEFAULT_BRANCH}"
        }
        agent {
            docker {
                label 'ci_label'
                alwaysPull true
                // NEXUS_REGISTRY_IP and NEXUS_REGISTRY_PORT are Jenkins global variables
                registryUrl "https://${env.NEXUS_REGISTRY_IP}:${env.NEXUS_REGISTRY_PORT}"
                registryCredentialsId 'nexusadmin'
                image "nrp:${IMG_TAG}"
                args '--entrypoint="" -u root --privileged'
            }
        }
        options { 
            // Skip code checkout prior running pipeline (only Jenkinsfile is checked out)
            skipDefaultCheckout true
        }

        stages {
            stage('Code checkout') {
                steps {
                    // Debug information on available environment
                    echo sh(script: 'env|sort', returnStdout: true)
                    sh 'sudo chown -R 1000:1000 * || echo "no files to change"'


                    // Checkout main project to GIT_CHECKOUT_DIR
                    cloneSCM(env.GIT_CHECKOUT_DIR)

                    // Clone all dependencies
                    cloneDepTopic(env.USER_SCRIPTS_DIR, 'git@bitbucket.org:hbpneurorobotics/user-scripts.git', env.TOPIC_BRANCH, env.DEFAULT_BRANCH)
                    sh "git config --global --add safe.directory '*'"
                }
            }
            
            stage('Build') {
                steps {
                    // Build operations (starting in .ci directory)
                    dir(env.GIT_CHECKOUT_DIR){
                        // Determine explicitly the shell as bash (needed for proper user-scripts operation)
                        sh 'bash .ci/build.bash'
                    }
                }
            }
            
            stage('Push to Nexus') {
                steps {
                    sh "cp ${env.GIT_CHECKOUT_DIR}/build/*.deb package.deb"

                    // The credentials are store in jenkins configuration
                    httpRequest url:    "${env.NEXUS_APT_FOCAL}",
                                        authentication: 'apt-user',
                                        httpMode: 'POST',
                                        uploadFile: "package.deb",
                                        contentType: 'APPLICATION_OCTETSTREAM',
                                        multipartName: "package.deb",
                                        wrapAsMultipart: false
                }
            }
        }

        post {
            always {
                script{sh 'sudo rm -rf *'}
                cleanWs()
            }
        }
    }
}