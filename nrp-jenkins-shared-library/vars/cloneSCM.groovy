def call(folder) {
// cloneSCM: 
//      1 - directory to checkout
    dir(folder) {
        checkout([
            $class: "GitSCM",
            branches: scm.branches, 
            extensions: [
                [$class: 'CloneOption', noTags: false],
                [$class: 'LocalBranch', localBranch: "**"],
                [$class: 'CleanBeforeCheckout']
            ], 
            userRemoteConfigs: scm.userRemoteConfigs
        ])
    }
    sh "git config --global --add safe.directory '*'"
}