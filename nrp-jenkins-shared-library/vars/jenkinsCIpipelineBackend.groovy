def call(targetRepository="ExDBackend"){

    // Before starting pipeline, we try to get the proper image tag
    def DEFAULT_BRANCH = 'development'
    // selectTopicBranch function is used to choose the correct branch name as topic
    // the function is defined in shared libs
    // 
    // In case there is a PR for a branch, then Jenkins runs a pipeline for this pull request, not for the branch, 
    // even if there are new commits to the branch (until PR is not closed). The BRANCH_NAME variable in this case is something like PR-###
    // The name of the branch which is merged is stored in CHANGE_BRANCH variable. Thus, we should choose CHANGE_BRANCH as topic
    //
    // If there is a branch without PR, then Jenkins creates build for it normally for every push and the branch name is stored in BRANCH_NAME variable.
    // CHANGE_BRANCH is empty in this case. Thus, we choose BRANCH_NAME as topic for branches without PR.
    def TOPIC_BRANCH = selectTopicBranch(env.BRANCH_NAME, env.CHANGE_BRANCH)
    // We try to pull the image with the topic name, or use default tag otherwise
    def IMG_TAG = checkImageTag("${TOPIC_BRANCH}", "${DEFAULT_BRANCH}")

    def backendRepositories = [
        [
            "location": "git@bitbucket.org:hbpneurorobotics/ExperimentControl.git",
            "directory": "ExperimentControl"
        ],
        [
            "location": "git@bitbucket.org:hbpneurorobotics/CLE.git",
            "directory": "CLE"
        ],
        [
            "location": "git@bitbucket.org:hbpneurorobotics/ExDBackend.git",
            "directory": "ExDBackend"
        ],
        [
            "location": "git@bitbucket.org:hbpneurorobotics/BrainSimulation.git",
            "directory": "BrainSimulation"
        ]
    ]

    pipeline {
        environment {
            USER_SCRIPTS_DIR = "user-scripts"
            ADMIN_SCRIPTS_DIR = "admin-scripts"
            GAZEBO_ROS_DIR = "GazeboRosPackages"
            EXP_CONTROL_DIR = "ExperimentControl"
            CLE_DIR = "CLE"
            BRAIN_SIMULATION_DIR = "BrainSimulation"
            EXDBACKEND_DIR = "ExDBackend"
            // GIT_CHECKOUT_DIR is a dir of the main project (that was pushed)
            GIT_CHECKOUT_DIR = "${targetRepository}"

            // That is needed to pass the variables into environment with the same name from 
            // Jenkins global scope (def ..=..)
            TOPIC_BRANCH = "${TOPIC_BRANCH}"
            DEFAULT_BRANCH = "${DEFAULT_BRANCH}"
        }
        agent {
            docker {
                label 'ci_label'
                alwaysPull true
                // NEXUS_REGISTRY_IP and NEXUS_REGISTRY_PORT are Jenkins global variables
                registryUrl "https://${env.NEXUS_REGISTRY_IP}:${env.NEXUS_REGISTRY_PORT}"
                registryCredentialsId 'nexusadmin'
                image "nrp:${IMG_TAG}"
                args '--entrypoint="" -u root --privileged'
            }
        }
        options { 
            // Skip code checkout prior running pipeline (only Jenkinsfile is checked out)
            skipDefaultCheckout true
        }

        stages {
            stage('Code checkout') {
                steps {
                    // Debug information on available environment
                    echo sh(script: 'env|sort', returnStdout: true)
                    sh 'sudo chown -R 1000:1000 * || echo "no files to change"'

                    script{
                        backendRepositories.each {repo ->
                            if (targetRepository == repo.location) {
                                // Checkout main project to targetRepository
                                cloneSCM(targetRepository)
                            } else {
                                cloneDepTopic(repo.directory, repo.location, env.TOPIC_BRANCH, env.DEFAULT_BRANCH)
                            }
                        }
                    }

                    // Clone all dependencies
                    // cloneRepoTopic: 
                    //      1 - directory to checkout
                    //      2 - repo
                    //      3 - name of topic branch
                    //      4 - default branch if topic unavailable
                    //      5 - username for chown
                    cloneDepTopic(env.ADMIN_SCRIPTS_DIR,   'git@bitbucket.org:hbpneurorobotics/admin-scripts.git',     env.TOPIC_BRANCH, env.DEFAULT_BRANCH)
                    cloneDepTopic(env.USER_SCRIPTS_DIR,    'git@bitbucket.org:hbpneurorobotics/user-scripts.git',      env.TOPIC_BRANCH, env.DEFAULT_BRANCH)
                    cloneDepTopic(env.GAZEBO_ROS_DIR,      'git@bitbucket.org:hbpneurorobotics/gazeborospackages.git', env.TOPIC_BRANCH, env.DEFAULT_BRANCH)

                    sh "git config --global --add safe.directory '*'"
                }
            }

            stage('Build GazeboRosPackages') {
                steps {
                    // Use GazeboRosPackages build script
                    dir(env.GAZEBO_ROS_DIR){
                        // Determine explicitly the shell as bash (needed for proper user-scripts operation)
                        sh 'bash .ci/build.bash'
                    }
                    
                }
            }
            stage('Build and test') {
                steps {
                    dir(env.GIT_CHECKOUT_DIR){
                        // Call the build script with HBP as WORKSPACE
                        sh "export HBP=${WORKSPACE} && bash .ci/build.bash"

                        // deliver artifacts
                        makeReports(true)
                    }
                }
            }
        }

        post {
            always {
                dir(env.GIT_CHECKOUT_DIR){
                    archiveArtifacts 'p*.*'
                    archiveArtifacts 'test-reports/*.*'
                    archiveArtifacts 'coverage.xml'
                }
            }
        }
    }

}