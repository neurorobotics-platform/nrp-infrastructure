# Controlling Castor with CLI

This folder contains files that should help to maintain VMs at Castor through CLI.

## Using Docker image for running CLI

There is a Docker file for building the image that contains the necessary software for controlling Castor Openstack. It is possible to work without this image, but all software, including environmental changes, should be applied to your system then.

Build the image as usual with context located in this folder.

```bash
docker build -f ubuntu-openstack-castor.Dockerfile -t ubuntu-openstack-castor:latest .
```

Run the container interactively from this image, better mounting the scripts folder into the container.

```bash
docker run -v scripts:/root/scripts -it ubuntu-openstack-castor:latest
```

Inside the container, source the Openstack env file

```bash
source castor-cli-otp.env
```

## Using Python scripts for maintaining VMs

### Creation of the frontend-backend groups

To create the new VM groups, use `env.up.py` script, use `--help` to list the creation options. I.e.:

```bash
env.up.py --env test --purpose testing --nbackend 3
```

will create 4 VMs: 3 backends and 1 frontend with `env=test` and `testing` purpose metadata. 


The folowing example will create the env for developer:

```bash
./env.up.py --purpose developers --frontend-volume --frontend-volume-size 20 --force --env newdeveloper

```

### Generation of the inventory file for the Ansible

```bash
updateInventory.py
```

This scripts generated 'inventory.json' file that can be used in Ansible. This file should be placed to `ansible\inventories\nrp\hosts.json` to make it working with the Ansible playbooks in `nrp-infrastructure`.