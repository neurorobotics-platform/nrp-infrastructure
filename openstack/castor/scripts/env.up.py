#!/usr/bin/python3

import json
from argparse import ArgumentParser
from os import name
import NRPOpenstackController


ENV_NAME="test"
PURPOSE = "testing"

FLAVOR_B="m1.large"
FLAVOR_F="m1.medium"
FLAVOR_T="m1.small2"
SSH_KEY="castor-test"

parser = ArgumentParser()
parser.add_argument("--env", dest="env", default=ENV_NAME,
                    help="the name of the new env", metavar="ENV")
parser.add_argument("--purpose", dest="purpose", default=PURPOSE,
                    help="the meta 'purpose' value", metavar="PURPOSE")
parser.add_argument("--nbackend", dest="nbackend", default=1,
                    help="the number of backends", metavar="N")
parser.add_argument("--backend-flavor", dest="backend_flavor", default="m1.large",
                    help="the name of openstack compute flavor of backend", metavar="STR")
parser.add_argument("--nfrontend", dest="nfrontend", default=1,
                    help="the number of frontends", metavar="N")
parser.add_argument("--frontend-flavor", dest="frontend_flavor", default="m1.medium",
                    help="the name of openstack compute flavor of frontend", metavar="STR")
parser.add_argument("--ntunnel", dest="ntunnel", default=0,
                    help="the number of tunneling VMs", metavar="N")

parser.add_argument("--tunnel-flavor", dest="tunnel_flavor", default="m1.small2",
                    help="the name of openstack compute flavor of tunnel", metavar="STR")

parser.add_argument("--frontend-volume", dest="frontend_volume", action='store_true',
                    help="should we attach volume to the frontend?")
parser.add_argument("--frontend-volume-size", dest="frontend_volume_size", default='20',
                    help="the size of the frontend volume", metavar="N")
parser.add_argument("--force", dest="force", action='store_true',
                    help="force recreation of instances (any instances with existing 'env' will be deleted)")

args = parser.parse_args()

ENV_NAME = args.env
PURPOSE = args.purpose

print("Bringing up env {} for {} using".format(ENV_NAME, PURPOSE))

N_BACK = int(args.nbackend)
N_FRONT = int(args.nfrontend)

FLAVOR_B = str(args.backend_flavor)
FLAVOR_F = str(args.frontend_flavor)
FLAVOR_T = str(args.tunnel_flavor)

FRONTEND_VOLUME = args.frontend_volume
FRONTEND_VOLUME_SIZE = args.frontend_volume_size
FORCE_RECREATION = args.force


if '-' in ENV_NAME or '.' in ENV_NAME:
    print('env name should not contain "-" or "." ({})'.format(ENV_NAME))
    exit(100)

if N_FRONT > 1:
    print('This script is not designed for creating multiple frontends')
    exit(100)

controler = NRPOpenstackController.NRPOpenstackController('WARNING')

import os
if not os.path.exists("initlogs"):
    os.makedirs("initlogs")

# controler.conn.create_server_group('ServerGroup-' + ENV_NAME, policy='affinity')

if FORCE_RECREATION:
    servers = controler.getServersByEnv(ENV_NAME)
    for server in servers:
        print("Deleting server {}...".format(server['id']))
        response = controler.conn.delete_server(server['id'], wait=True)
        print(response)
    volumes = controler.getVolumesByEnv(ENV_NAME)
    for volume in volumes:
        print("Deleting volume {}...".format(volume['id']))
        controler.conn.delete_volume(volume['id'], wait = True)

for i in range(N_BACK):
    print("Launching backend {}...".format(i))
    server = controler.createBackend(env=ENV_NAME, numi=(i+1), purpose=PURPOSE, flavor=FLAVOR_B)

    # with open('initlogs/' + server['name'] + '.ini.json', 'w') as f:
    #     json.dump(server, f, indent=4)

frontend_volume = None
if FRONTEND_VOLUME:
    volumes = controler.getVolumesByEnv(ENV_NAME)
    if volumes:
        if len(volumes) > 1:
            print("More than one volume found, usin the first {}".format(json.dumps(volumes)))
        frontend_volume = volumes[0]
    else:
        volumeName = ENV_NAME + '_volume'
        print("Creaing volume {}...".format(volumeName))
        frontend_volume = controler.conn.create_volume(size=FRONTEND_VOLUME_SIZE, name=volumeName, wait=True)
        # with open('initlogs/' + frontend_volume['name'] + '.ini.json', 'w') as f:
        #     json.dump(frontend_volume, f, indent=4)

for i in range(N_FRONT):
    print("Launching frontend {}...".format(i))
    server = controler.createFrontend(env=ENV_NAME, numi=(i+1), purpose=PURPOSE, flavor=FLAVOR_F)
    if FRONTEND_VOLUME:
        if len(server['volumes']) == 1:
            print('Attaching volume {} to instance {}'.format(frontend_volume['name'], server['name']))
            frontend_volume = controler.conn.attach_volume(server, frontend_volume, device='/dev/sdb', wait=True)
        elif not any(vol['id'] == frontend_volume['id'] for vol in server['volumes']):
            print('Server has attached volumes, but {} is not among them.'.format(frontend_volume['id']))
        else:
            print('Volume is already attached')

    # with open('initlogs/' + server['name'] + '.ini.json', 'w') as f:
    #     json.dump(server, f, indent=4)


for i in range(int(args.ntunnel)):
    print("Launching tunnel {}...".format(i))
    server = controler.createTunnel(env=ENV_NAME, numi=(i+1), purpose=PURPOSE, flavor=FLAVOR_T)

    # with open('initlogs/' + server['name'] + '.ini.json', 'w') as f:
    #     json.dump(server, f, indent=4)
