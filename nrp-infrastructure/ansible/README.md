## Requirements 

* sudo apt-add-repository ppa:ansible/ansible
* sudo apt install ansible # >= 2.9
* ansible-galaxy collection install community.docker


## Playbooks

### Setting NRP 3.x

Set all required inventory variables in `ansible/inventories/nrp` and define the inventory itself, if needed.

1. Updating the instance

The playbook installs necessary software on the Virtual Machines and performs their setup.

```bash
ansible-playbook -i ansible/inventories/nrp ansible/nrp_update.yml -l <hosts_to_execute> --key-file <ssh_key>
```

2. Cleaning of the instance

The playbook removes all docker images and containers

```bash
ansible-playbook -i ansible/inventories/nrp ansible/nrp_clean.yml -l <hosts_to_execute> --key-file <ssh_key>
```

3. Getting the proper NRP code 

The playbook downloads the NRP image with the provided tag name and models&experiments with provided branch name

```bash
ansible-playbook -i ansible/inventories/nrp ansible/nrp_install.yml -l <hosts_to_execute> --key-file <ssh_key> -e docker_tag=<tag> -e branch_name=<branch> -e base_name=development -e docker_reg=nexus.neurorobotics.ebrains.eu:443 
```

4. Running the NRP

The playbook runs the containers with proper settings

```bash
ansible-playbook -i ansible/inventories/nrp ansible/nrp_run.yml -l <hosts_to_execute> --key-file <ssh_key> -e docker_tag=<tag> -e branch_name=<branch> -e base_name=development -e docker_reg=nexus.neurorobotics.ebrains.eu:443 -e ssh_key_path=<path_to_ssh_key>
```


### Setting NRP 4.x

Set all required inventory variables in `ansible/inventories/nrp4` and define the inventory itself, if needed.

The example of the NRP 4 inventory:

```json
{
    "all": {
        "hosts": {
            "148.187.149.212": {
                "internal_ip": "10.1.1.250",
                "env": "prod_latest",
                "type": "frontend",
                "vmname": "prod_latest_frontend-1"
            },
            "148.187.148.241": {
                "internal_ip": "10.1.1.167",
                "env": "prod_latest",
                "type": "backend",
                "vmname": "prod_latest_backend-2"
            },
            "148.187.148.192": {
                "internal_ip": "10.1.1.142",
                "env": "prod_latest",
                "type": "backend",
                "vmname": "prod_latest_backend-1"
            }
        },
        "vars": {},
        "children": {
            "all_backends": {
                "children": {
                    "prod_latest_backends": null
                },
                "vars": {}
            },
            "all_frontends": {
                "children": {
                    "prod_latest_frontends": null
                },
                "vars": {}
            },
            "production_backends": {
                "children": {
                    "prod_latest_backends": null
                },
                "vars": {}
            },
            "production_frontends": {
                "children": {
                    "prod_latest_frontends": null
                },
                "vars": {}
            },
            "production_tunnels": {
                "children": {
                    "prod_latest_tunnels": null
                },
                "vars": {}
            },
            "production": {
                "hosts": {
                    "148.187.149.212": null,
                    "148.187.148.241": null,
                    "148.187.148.192": null
                }
            },
            "prod_latest_backends": {
                "hosts": {
                    "148.187.148.241": null,
                    "148.187.148.192": null
                },
                "vars": {
                    "frontend_group": "prod_latest_frontends",
                    "internal_frontend_ip": "10.1.1.250"
                }
            },
            "prod_latest_frontends": {
                "hosts": {
                    "148.187.149.212": null
                },
                "vars": {
                    "backend_group": "prod_latest_backends",
                    "backend_hosts": {
                        "prod_latest_backend-2": {
                            "public_ip": "148.187.148.241",
                            "private_ip": "10.1.1.167"
                        },
                        "prod_latest_backend-1": {
                            "public_ip": "148.187.148.192",
                            "private_ip": "10.1.1.142"
                        }
                    },
                    "tunnel_hosts": {}
                }
            },
            "prod_latest": {
                "hosts": {
                    "148.187.149.212": null,
                    "148.187.148.241": null,
                    "148.187.148.192": null
                }
            }
        }
    }
}
```

1. Updating the instance

The playbook installs necessary software on the Virtual Machines and performs their setup. See as an exmple, `examples/update-and-run-nrp4.sh`

```bash
ansible-playbook \
    -l <hosts_to_execute> 
    -i ansible/inventories/nrp4 ansible/nrp_update.yml \
    --key-file <ssh_key> \
    -e ssh_key_path=<ssh_key> \
    -e docker_tag=<tag> \
    -e branch_name=<branch> \
    -e base_name=development -e docker_reg=nexus.neurorobotics.ebrains.eu:443 
```

2. Getting the proper NRP code 

The playbook downloads the NRP image with the provided tag name and models&experiments with provided branch name

```bash
ansible-playbook \
    -l <hosts_to_execute> \
    -i ansible/inventories/nrp4 ansible/nrp4_install.yml \
    --key-file <ssh_key> \
    -e ssh_key_path=<ssh_key> \
    -e docker_tag=<tag> \
    -e branch_name=<branch> \
    -e base_name=development
```

4. Running the NRP

The playbook runs the containers with proper settings

```bash
ansible-playbook \
    -l <hosts_to_execute> \
    -i ansible/inventories/nrp4 ansible/nrp4_run.yml \
    --key-file <ssh_key> \
    -e ssh_key_path=<ssh_key> \
    -e docker_tag=<tag> \
    -e branch_name=<branch> \
    -e base_name=development
```

Make sure to provide the necessary variables, like OIDC client secrets.
