-- required imports
local cjson = require "cjson"
local collab2 = require("nrp-ebrains")


-- commom variables
-- how long the cache will stay active, in seconds
local cache_time = 60
local oauth_cache = ngx.shared.oauth_cache
local refresh_token = 20 * 60 * 60 -- 20 hours token refresh time

--from the nginx config
local client_scope = ngx.var.oidc_client_scope
--TODO: Remove this once we are using scopes properly
client_scope = client_scope or 'openid'
local accept_client_auth = ngx.var.accept_client_auth


-- functions
function set_cache(cache_value)
    local succ, err, forcible = oauth_cache:set(auth_token, cache_value, cache_time)
    if err then
       ngx.log(ngx.DEBUG, "Error Adding to cache: "..err)
    end
end


-- request processing

-- get token from the header
local auth_header = ngx.req.get_headers()["Authorization"]
if not auth_header then
    ngx.status = ngx.HTTP_UNAUTHORIZED
    ngx.say('{"status": 401, "message": "No Authorization header"}')
    return ngx.exit(ngx.HTTP_OK)
end

local pars = string.gmatch(auth_header, "%S+")
local bearer, auth_token, extra = pars(), pars(), pars()
if not auth_token or extra or not bearer or 'bearer' ~= string.lower(bearer) then
    ngx.log(ngx.DEBUG, "Missing Bearer: "..auth_header)
    ngx.status = ngx.HTTP_UNAUTHORIZED
    ngx.say('{"status": 401, "message": "Incorrect Authorization header"}')
    return ngx.exit(ngx.HTTP_OK)
end


-- check if we're in the cache, Note: a user may have a invalid token
-- during the period that cache_time is set to, and still be considered valid
local cached_info = oauth_cache:get(auth_token)
if cached_info then
   local user_info = cjson.decode(cached_info)
--    ngx.log(ngx.DEBUG, "user_info: "..user_info.type)
   if user_info.type == 'user_auth' then
      collab2.set_user_header(user_info)
   end
   return
end

local res = {}
local cached_response_body = oauth_cache:get("cached_response_body")
if cached_response_body == nil or cjson.decode(cached_response_body).exp ~= nil and cjson.decode(cached_response_body).exp + refresh_token - ngx.now() < 0 then
    -- if we do not have cache, check token and save to cache
    res = collab2.introspect_token(auth_token)
    local succ, err, forcible = oauth_cache:set("cached_response_body",res.body)
    local succ, err, forcible = oauth_cache:set("status",res.status)
else
    res.body =  oauth_cache:get("cached_response_body") 
    res.status = oauth_cache:get("status")
end
if res.status ~= 200 then  
    ngx.status = res.status
    ngx.say(res.body)
    ngx.exit(ngx.HTTP_OK)
end

-- check that token is still active
-- ngx.log(ngx.DEBUG, "OpenId introspect response: "..res.body)
local json = cjson.decode(res.body)
if not json.active then
    ngx.status = ngx.HTTP_UNAUTHORIZED
    ngx.say('{"status":401, "message": "OpenId response: token is not valid"}')
    ngx.exit(ngx.HTTP_OK)
end

-- check if the string `scopes` contains a match to our client_scope
-- returns if allowed, otherwise, signals that this is unauthorized,
-- and doesn't return
function check_scope(client_scope, scopes)
    for scope in scopes:gmatch("%S+") do
        if client_scope == scope then
            return true
        end
    end
    return false
end
if not check_scope(client_scope, json.scope) then
    ngx.log(ngx.DEBUG, "failed check_scope: "..json.scope)
    ngx.status = ngx.HTTP_UNAUTHORIZED
    ngx.say('{"status": 401, "message": "Not in scope: '..client_scope..'"}')
    return ngx.exit(ngx.HTTP_OK)
end

-- if client authentication is accepted and json.sub matches the clientid
-- and not the user id as for user triggered requests
if accept_client_auth and json.clientId == json.client_id then
    client_info = cjson.encode({type = "client_auth"})
    set_cache(client_info)
    return -- we skip getting the user information
end

-- get user info
user_info = collab2.get_user(auth_token)

set_cache(cjson.encode(user_info))

collab2.set_user_header(user_info)
