local cjson = require "cjson"
local http2 = require("resty.http")

-- how long the cache will stay active, in seconds
local cache_time = 60
local oauth_cache = ngx.shared.oauth_cache
local refresh_token = 20 * 60 * 60 -- 20 hours token refresh time

local max_sub_request_time = ngx.var.max_sub_request_time
max_sub_request_time = max_sub_request_time or 30

local _M = {}

function _M.get_user(token)
    local auth_string = "Bearer "..token
    local headers = {
        ["Authorization"] = auth_string
    }
    
    local httpc = http2.new()
    local res, err = httpc:request_uri(ngx.var.userinfo_endpoint, 
    {
        method = "GET",
        headers = headers,
        ssl_verify = false,
        keepalive = false
    })

    if res.status ~= 200 then
        ngx.log(ngx.DEBUG, "User-info request to userinfo_endpoint failed.")
        ngx.status = res.status
        ngx.say(res.body)
        ngx.exit(ngx.HTTP_OK)
    end

    local user_info = cjson.decode(res.body)
    if not user_info["mitreid-sub"] then
        ngx.log(ngx.DEBUG, res.body)
        ngx.log(ngx.DEBUG, token)
        ngx.log(ngx.DEBUG, "Got nil mitreid-sub form userinfo request")
        ngx.status = ngx.HTTP_UNAUTHORIZED
        ngx.say('{"status":401, "message": "Got nil mitreid-sub form userinfo request"}')
        ngx.exit(ngx.HTTP_OK)
    end

    user_info.type = 'user_auth'

    return user_info
end

function _M.introspect_token(token_to_check)
    local start_time = ngx.now()

    local reqbody = { 
        client_id = ngx.var.oidc_client_id, 
        client_secret = ngx.var.oidc_client_secret, 
        token = token_to_check
    }

    local headers = {
                ["Content-Type"] = "application/x-www-form-urlencoded"
            }

    local httpc = http2.new()
    local res, err = httpc:request_uri(ngx.var.introspect_endpoint, 
    {
        method = "POST",
        body = ngx.encode_args(reqbody),
        headers = headers,
        ssl_verify = false,
        keepalive = false
      })

    if ngx.now() - start_time > max_sub_request_time then
        ngx.log(ngx.DEBUG, 'Sub request to '..url..' took a long time: '..(ngx.now() - start_time))
    end

    return res
end


function _M.set_user_header(user_info)
    ngx.req.set_header("X-User-Name", user_info["mitreid-sub"])
 end

return _M
