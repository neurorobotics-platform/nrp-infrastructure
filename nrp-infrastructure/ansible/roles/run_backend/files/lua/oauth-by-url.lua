local cjson = require "cjson"
local collab2 = require("nrp-ebrains")

-- how long the cache will stay active, in seconds
local cache_time = 300
local oauth_cache = ngx.shared.oauth_cache

-- probably params from ~/.bbp_services.cfg should be used
local client_id = ngx.var.oidc_client_id
local client_secret = ngx.var.oidc_client_secret


-- functions
function set_cache(cache_value)
    local succ, err, forcible = oauth_cache:set(auth_token, cache_value, cache_time)
    if err then
       ngx.log(ngx.DEBUG, "Error Adding to cache: "..err)
    end
end

-- request processing
-- get token from the url:
local auth_token = ngx.var.arg_token

-- check if a valid token present
if not auth_token or auth_token == 'undefined' or token == 'no-token' then
    ngx.status = ngx.HTTP_UNAUTHORIZED
    ngx.log(ngx.DEBUG, "Missing url token args")
    ngx.say('{"status": 401, "message": "Missing url token args"}')
    return ngx.exit(ngx.HTTP_OK)
end

--check if we're in the cache
local cached_info = oauth_cache:get(auth_token)
if cached_info ~= nil then
   local user_info = cjson.decode(cached_info)
   collab2.set_user_header(user_info)
   return
end

-- check token status
local res = collab2.introspect_token(auth_token)
if res.status ~= 200 then
    ngx.status = res.status
    ngx.say(res.body)
    ngx.exit(ngx.HTTP_OK)
end

-- ngx.log(ngx.DEBUG, "OpenId introspect response: "..res.body)
local json = cjson.decode(res.body)
if not json.active then
    ngx.status = ngx.HTTP_UNAUTHORIZED
    ngx.say('{"status":401, "message": "OpenId response: token is not valid"}')
    ngx.exit(ngx.HTTP_OK)
end

-- get userinfo
user_info = collab2.get_user(auth_token)

set_cache(cjson.encode(user_info))

collab2.set_user_header(user_info)
