#!/usr/bin/env bash

# Usage ./restart-server [servername]
#    eg: ./restart-server.sh 148.187.82.140-port-80
# Where [servername] is in the form {ip}-port-{port}
#
# The script will connect to the specified {ip} through ssh and restart supervisor in the container

hostArr=(${1//-port-/ })
case "${hostArr[1]}" in
 80) container="nrp" ;;
 *) container="nrp_2" ;;
esac

host="${hostArr[0]}"
ssh -o StrictHostKeyChecking=no -i $HOME/nrp/src/test-key.pem "ubuntu@${host}" 'sudo docker exec '"${container}"' bash -c "sudo /etc/init.d/supervisor stop && sleep 5 && sudo /etc/init.d/supervisor start"'
