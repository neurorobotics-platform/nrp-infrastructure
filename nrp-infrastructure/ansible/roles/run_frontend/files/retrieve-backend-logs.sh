#!/usr/bin/env bash

# Usage ./retrieve-backend-logs.sh [servername] [targetfile]
#    eg: ./retrieve-backend-logs.sh 148.187.82.140-port-80 /tmp/backend_logs/log.tar.gz
# Where [servername] is in the form {ip}-port-{port}
#
# The script will connect to the specified {ip} through ssh and
# download the logs as a zip

SSH_KEY=$HOME/nrp/src/test-key.pem
SSH_ARGS="-o StrictHostKeyChecking=no -i ${SSH_KEY}"

hostArr=(${1//-port-/ })
case "${hostArr[1]}" in
 80) container="nrp" ;;
 *) container="nrp_2" ;;
esac

host="${hostArr[0]}"

target=${2}
target_dir=$(dirname -- "$target")
filename=$(basename -- "$target")
filename="${filename%%.*}"

log_dir_name="logs_${host}_${container}"

ssh ${SSH_ARGS} "ubuntu@${host}" "
sudo rm -rf /tmp/${filename}
sudo mkdir -p /tmp/${filename}/${log_dir_name}
sudo docker cp ${container}:/var/log/supervisor /tmp/${filename}/${log_dir_name}
sudo docker cp ${container}:/var/log/nginx /tmp/${filename}/${log_dir_name}
cd /tmp
sudo tar czf  /tmp/${filename}.tar.gz -C /tmp/${filename} ${log_dir_name}
sudo rm -rf /tmp/${filename}
"

mkdir -p ${target_dir}
scp ${SSH_ARGS} ubuntu@${host}:/tmp/${filename}.tar.gz ${target}
ssh ${SSH_ARGS} "ubuntu@${host}" "sudo rm -rf /tmp/${filename}.tar.gz"
