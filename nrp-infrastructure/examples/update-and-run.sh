#!/bin/bash

TARGET_ENV="development"
IMAGE_TAG="development"
BRANCH="development"
BASE_BRANCH="development"

SSH_KEY=/path/to/key

# We need to switch to the repository root directory.
cd $(dirname "$0")/..

# update VMs
ansible-playbook \
    -l "${TARGET_ENV}" \
    -i ansible/inventories/nrp ansible/nrp_update.yml \
    --key-file "${SSH_KEY}" \
    --forks 10 \
    -e ssh_key_path="${SSH_KEY}" \
    -e docker_tag="${IMAGE_TAG}" \
    -e branch_name="${BRANCH}" \
    -e base_name="${BASE_BRANCH}"

# install nrp
ansible-playbook \
    -l "${TARGET_ENV}" \
    -i ansible/inventories/nrp ansible/nrp_install.yml \
    --key-file "${SSH_KEY}" \
    -e ssh_key_path="${SSH_KEY}" \
    --forks 10 \
    -e docker_tag="${IMAGE_TAG}" \
    -e branch_name="${BRANCH}" \
    -e base_name="${BASE_BRANCH}"

# run nrp
ansible-playbook \
    -l "${TARGET_ENV}" \
    -i ansible/inventories/nrp ansible/nrp_run.yml \
    --key-file "${SSH_KEY}" \
    -e ssh_key_path="${SSH_KEY}" \
    --forks 10 \
    -e docker_tag="${IMAGE_TAG}" \
    -e branch_name="${BRANCH}" \
    -e base_name="${BASE_BRANCH}" \
    -e running_mode=2