def call(autoupdate, cobertura_coverage_line = "0"){
    catchError(buildResult: 'UNSTABLE', stageResult: 'UNSTABLE', message: 'Test coverage has dropped') {
        step([$class: 'CoberturaPublisher', 
            autoUpdateHealth: true, 
            autoUpdateStability: autoupdate, 
            coberturaReportFile: 'coverage.xml', 
            failUnhealthy: false, 
            failUnstable: true, 
            maxNumberOfBuilds: 0, 
            onlyStable: false, 
            sourceEncoding: 'ASCII', 
            zoomCoverageChart: false,
            lineCoverageTargets: "0.0, 0, ${cobertura_coverage_line}"])
    }
    junit(allowEmptyResults: true, testResults: 'test-reports/*.xml')
    recordIssues enabledForFailure: true, tools: [pyLint(pattern: 'pylint.txt'), pep8(pattern: 'pycodestyle.txt|pep8.txt')], qualityGates: [[threshold: 1, type: 'TOTAL', unstable: true]]
}