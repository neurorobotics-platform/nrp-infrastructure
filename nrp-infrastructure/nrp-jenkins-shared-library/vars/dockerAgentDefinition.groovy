def call(imgTag="development", workerLabel="ci_label"){
        docker {
            label "${workerLabel}"
            alwaysPull true
            // NEXUS_REGISTRY_IP and NEXUS_REGISTRY_PORT are Jenkins global variables
            registryUrl "https://${env.NEXUS_REGISTRY_IP}:${env.NEXUS_REGISTRY_PORT}"
            registryCredentialsId 'nexusadmin'
            image "nrp_frontend:${imgTag}"
            args '--entrypoint="" -u root --privileged'
    }
}