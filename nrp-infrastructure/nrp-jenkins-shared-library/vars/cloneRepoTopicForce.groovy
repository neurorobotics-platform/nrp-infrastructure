def call(folder, repoUrl, topicBranch, defaultBranch, user) {
// cloneRepoTopic: 
//      1 - directory to checkout
//      2 - repo
//      3 - name of topic branch
//      4 - default branch if topic unavailable
//      5 - username for chown
    dir(folder) {
        try {
            echo "${folder}: Trying to checkout branch ${topicBranch}."
            checkout([
                $class: "GitSCM",
                branches: [[name: topicBranch]], 
                extensions: [], 
                userRemoteConfigs: [[
                    credentialsId: "${GIT_SSH_KEY}", 
                    url: repoUrl
                ]]
            ])
        }
        catch (e) {
            echo "${folder}: Branch ${topicBranch} is not available, getting ${defaultBranch} instead."
            checkout([
                $class: "GitSCM",
                branches: [[name: defaultBranch]], 
                extensions: [], 
                userRemoteConfigs: [[
                    credentialsId: "${GIT_SSH_KEY}", 
                    url: repoUrl
                ]]
            ])
        }
        sh "chown -R ${user} ./"
    }
}