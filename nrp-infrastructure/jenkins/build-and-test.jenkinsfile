//////////////////////////////////////////
// Pipeline
pipeline
{
    agent {
        label 'jenkins_master'
    }

    options {
        ansiColor('xterm')
        disableConcurrentBuilds()
    }
    parameters {
      string(defaultValue: 'development', name: 'BRANCH', description: 'Feature branch for building', trim: true)
    }

    stages
    {

        stage('Build and deploy image')
        {
            steps
            {
                script {
                    def build = build(job: '/Castor_Admin_CD', parameters: [
                        string(name: 'NRP_INFRASTRUCTURE_REF_BRANCH', value: "origin/master"), 
                        string(name: 'Repository', value: "nrpDocker"), 
                        string(name: 'BRANCH_NAME', value: "${params.BRANCH}"), 
                        string(name: 'BASE_BRANCH_NAME', value: "master"),
                        booleanParam(name: 'BUILD_IMAGE', value: true),
                        booleanParam(name: 'DEPLOY_IMAGE', value: true),
                        booleanParam(name: 'DEPLOY_FRONTEND', value: true),
                        booleanParam(name: 'DEPLOY_BACKEND', value: true),
                        booleanParam(name: 'CLEAN_VM', value: true),
                        booleanParam(name: 'UPDATE_VM', value: true),
                        string(name: 'env', value: "development")], wait: true)

                    copyArtifacts(projectName: '/Castor_Admin_CD', selector: specific("${build.number}"));
                }
            }
            post {
                failure {
                    script {
                        if (params.BRANCH == 'development')
                            slackSend color: "danger", message: "Failed to build and deploy 'development' image (${BUILD_URL})."
                    }
                }
            }
        }
        stage('Tests')
        {
            parallel 
            {
                stage('Analyze image')
                {                        
                    steps
                    {
                        script {
                            writeFile(file: "anchore_images", encoding: "UTF-8", text: readFile(file: "pushedImages.log", encoding: "UTF-8"))
                            anchore engineCredentialsId: 'anchore-engine', name: 'anchore_images', bailOnFail: false, engineRetries: "3000"
                        }
                    }
                    post {
                        failure {
                            script {
                                if (params.BRANCH == 'development')
                                    slackSend color: "warning", message: "Failed to analyze 'development' image (${BUILD_URL})."
                            }
                        }
                    }
                }
                stage('Robot Framework tests')
                {
                    steps
                    {
                        script{
                            def build = build(
                                job: 'Robot Framework Sandbox',
                                parameters: [
                                    string(name: 'STAGING', value: 'custom'),
                                    booleanParam(name: 'DEBUG_RUN', value: false),
                                    string(name: 'LOGIN_PAGE', value: 'https://test.neurorobotics.ebrains.eu/#/esv-private')
                                ]
                            );
                            copyArtifacts(projectName: 'Robot Framework Sandbox', selector: specific("${build.number}"));
                        }
                        step([
                            $class : 'RobotPublisher',
                            outputPath : 'results',
                            outputFileName : "output.xml",
                            reportFileName : 'report.html',
                            logFileName : 'log.html',
                            disableArchiveOutput : false,
                            passThreshold : 100.0,
                            unstableThreshold: 100.0,
                            otherFiles : "*.png",
                        ])
                    }
                    post {
                        failure {
                            script {
                                if (params.BRANCH == 'development')
                                    slackSend color: "danger", message: "Tests for 'development' image failed (${BUILD_URL})."
                            }
                        }
                    }
                }
            }
        }
    }
    post {
        success {
            cleanWs()
        }
    }
}