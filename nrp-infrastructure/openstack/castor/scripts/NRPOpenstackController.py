#!/usr/bin/python3

from contextlib import nullcontext
import openstack
import json
import logging

class NRPOpenstackController:

    DEFAULT_IMAGE = "Ubuntu-20.04"
    DEFAULT_FLAVOR_B="m1.large"
    DEFAULT_FLAVOR_F="m1.large"
    DEFAULT_FLAVOR_T="m1.small2"

    ADMIN_KEY="castor-test"

    SC_DEFAULT="3fbcc54e-c23d-474d-b9fb-fe84f55e90ab"
    SC_SSH="52a5bbd3-e6a9-471d-b3d0-a796362ac880"
    SC_BACKEND="2bc50134-d9e6-4340-be7d-f3344250242f"
    SC_FRONTEND="7a43f43f-db79-4933-b28a-0308c63cb69a"


    LOCAL_NETWORK="local-network"
    EXT_NETWORK="ext-net1"

    def __init__(self, level = logging.INFO):
        self.conn = openstack.connect()
        self.inv = {}
        logging.basicConfig(level=level)

    def clearInventory(self):
        self.inv = {
            "all": {
                "hosts": {},
                "vars": {},
                "children": {}
            }
        }

    def getFreeFloatingIP(self, ext_network_name):
        """Return the dictionary, describing the free floating IP."""

        # get all floatin IPs
        floating_ips = self.conn.list_floating_ips()
        # sort those, that do not have attached instances
        free_floating_ips = [ip['floating_ip_address'] for ip in floating_ips if not ip['fixed_ip_address'] ]
        logging.info('free_floating_ips\n%s', json.dumps(free_floating_ips, indent=2))

        # if there is no free floating IPs, create one in the ext_network_name
        if not free_floating_ips:
            res = self.conn.create_floating_ip(network=ext_network_name)
            logging.info('created free_floating_ip\n%s', json.dumps(res, indent=2))
            return res
        # otherwise return the first free IP
        else:
            return free_floating_ips[0]

    def getServersByEnv(self, env):
        """Return the list of servers with specified metadata key 'env' value"""

        servers = self.getServersByMetaKey("env", env)
        return servers

    def getServersByType(self, typ):
        """Return the list of servers with specified metadata key 'type' value"""

        servers = self.getServersByMetaKey("type", typ)
        return servers

    def getServersByMetaKey(self, key, value):
        """Return the list of servers with specified metadata key and its value"""

        servers = [server for server in self.getServersWithMetaKey(key) if server["metadata"][key] == value]
        return servers

    def getServersWithMetaKey(self, key):
        """Return the list of servers with specified metadata key"""

        servers = [server for server in self.conn.list_servers() if key in server["metadata"]]
        return servers

    def getVolumesByEnv(self, env):
        """Return the list of volumes with specified metadata key 'env' value"""

        volumes = [volume for volume in self.conn.list_volumes() if volume["name"] == env + '_volume']
        return volumes


    def createBackend(
        self, 
        env, 
        numi,
        image = DEFAULT_IMAGE,
        flavor = DEFAULT_FLAVOR_B,
        diskSize = '40',
        purpose = 'testing'
    ):
        """Create the new backend instance"""

        serverName = env + "_backend-" + str(numi)

        return self.createServer(
            serverName,
            image,
            flavor,
            diskSize,
            security_groups=[
                self.SC_DEFAULT,
                self.SC_SSH,
                self.SC_BACKEND
            ],
            meta={
                "env": env,
                "type": "backend",
                "purpose": purpose
            }
        )

    
    def createFrontend(
        self, 
        env, 
        numi,
        image = DEFAULT_IMAGE,
        flavor = DEFAULT_FLAVOR_F,
        diskSize = '40',
        purpose = 'testing'
    ):
        """Create the new frontend instance"""

        serverName = env + "_frontend-" + str(numi)

        return self.createServer(
            serverName,
            image,
            flavor,
            diskSize,
            security_groups=[
                self.SC_DEFAULT,
                self.SC_SSH,
                self.SC_FRONTEND
            ],
            meta={
                "env": env,
                "type": "frontend",
                "purpose": purpose
            }
        )

    
    def createTunnel(
        self, 
        env, 
        numi,
        image = DEFAULT_IMAGE,
        flavor = DEFAULT_FLAVOR_T,
        diskSize = '40',
        purpose = 'testing'
    ):
        """Create the new tunnel instance"""

        serverName = env + "_tunnel-" + str(numi)

        return self.createServer(
            serverName,
            image,
            flavor,
            diskSize,
            security_groups=[
                self.SC_DEFAULT,
                self.SC_SSH,
                self.SC_BACKEND
            ],
            meta={
                "env": env,
                "type": "tunnel",
                "purpose": purpose
            }
        )
    

    def createServer(
        self,
        serverName,
        image,
        flavor,
        diskSize,
        security_groups,
        meta
    ):
        server = self.conn.get_server(serverName)
        if server:
            return server

        server = self.conn.create_server(
            name=serverName,
            image=image,
            flavor=flavor,
            auto_ip=False,
            network=[self.LOCAL_NETWORK],
            reuse_ips=False,
            boot_from_volume=True,
            volume_size=diskSize,
            terminate_volume=True,
            key_name=self.ADMIN_KEY,
            security_groups=security_groups,
            meta=meta,
            wait=True
        )

        server = self.conn.add_ip_list(server, self.getFreeFloatingIP(self.EXT_NETWORK), wait=True)

        return server


    def emptyFrontendInvGroup(self, env):
        """Prepare empty frontends JSON descriptsion for Ansible with specified 'env'"""

        res = {
            "hosts": {},
            "vars": {
                "backend_group": self.getBackendInvGroupByEnv(env),
                "tunnel_group": self.getTunnelInvGroupByEnv(env),
                "backend_hosts": {},
                "tunnel_hosts": {}
            }
        }
        return res

    def emptyBackendInvGroup(self, env):
        """Prepare empty backends JSON descriptsion for Ansible with specified 'env'"""

        res = {
            "hosts": {},
            "vars": {
                "frontend_group": self.getFrontendInvGroupByEnv(env)
            }
        }
        return res


    def emptyTunnelInvGroup(self, env):
        """Prepare empty tunnels JSON descriptsion for Ansible with specified 'env'"""

        res = {
            "hosts": {},
            "vars": {
                "frontend_group": self.getFrontendInvGroupByEnv(env)
            }
        }
        return res


    def getFrontendInvGroupByEnv(self, env):
        """Return the frontend Ansible inventory group name by 'env' value"""

        return env + "_frontends"


    def getBackendInvGroupByEnv(self, env):
        """Return the backends Ansible inventory group name by 'env' value"""

        return env + "_backends"


    def getTunnelInvGroupByEnv(self, env):
        """Return the tunnels Ansible inventory group name by 'env' value"""

        return env + "_tunnels"


    def getServerInvGroup(self, env, typ):
        """Return the Ansible inventory group name by 'env' and 'type' values"""

        if typ == "backend":
            return self.getBackendInvGroupByEnv(env)
        elif typ == "frontend":
            return self.getFrontendInvGroupByEnv(env)
        elif typ == "tunnel":
            return self.getTunnelInvGroupByEnv(env)
        else:
            raise ValueError('Unknown VM type: {}'. format(typ))

    def addChildrenGroupByPurpose(self, env, purpose):
        """Add the Ansible inventory children group 'env' and 'purpose' values"""

        group_backend = self.getBackendInvGroupByEnv(env)
        group_frontend = self.getFrontendInvGroupByEnv(env)
        group_tunnel = self.getTunnelInvGroupByEnv(env)
        purpose_frontends = purpose + '_frontends'
        purpose_backends = purpose + '_backends'
        purpose_tunnels = purpose + '_tunnels'
        if purpose_frontends not in self.inv["all"]["children"]:
            self.inv["all"]["children"][purpose_backends] = {"children": {}, "vars": {}}
            self.inv["all"]["children"][purpose_frontends] = {"children": {}, "vars": {}}
            self.inv["all"]["children"][purpose_tunnels] = {"children": {}, "vars": {}}
        
        self.inv["all"]["children"][purpose_backends]["children"][group_backend] = None
        self.inv["all"]["children"][purpose_frontends]["children"][group_frontend] = None
        self.inv["all"]["children"][purpose_tunnels]["children"][group_tunnel] = None

    
    def checkInvGroup(self, server):
        """Check if server groups exist in the Ansible inventory and create empty one if needed"""

        env = server["metadata"]["env"]
        purpose = server["metadata"]["purpose"]
        group_backend = self.getBackendInvGroupByEnv(env)
        group_frontend = self.getFrontendInvGroupByEnv(env)
        group_tunnel = self.getTunnelInvGroupByEnv(env)
        if group_frontend not in self.inv["all"]["children"]:
            self.inv["all"]["children"][group_backend] = self.emptyBackendInvGroup(env)
            self.inv["all"]["children"][group_frontend] = self.emptyFrontendInvGroup(env)
            self.inv["all"]["children"][group_tunnel] = self.emptyTunnelInvGroup(env)
            self.addChildrenGroupByPurpose(env, "all")
            self.addChildrenGroupByPurpose(env, purpose)
        if env not in self.inv["all"]["children"]:
            self.inv["all"]["children"][env] = {"hosts" :{}}
        if purpose not in self.inv["all"]["children"]:
            self.inv["all"]["children"][purpose] = {"hosts" :{}}

    
    def fillServerInvVars(self, server):
        """Fill inventory parameters by server description got from Openstack CLI"""

        env = server["metadata"]["env"]
        typ = server["metadata"]["type"]
        group_frontend = self.getFrontendInvGroupByEnv(env)
        group_backend = self.getBackendInvGroupByEnv(env)
        group_tunnel = self.getTunnelInvGroupByEnv(env)
        internal_ip = server["private_v4"]
        ip = server["public_v4"]
        name = server["name"]
        print(internal_ip)

        if typ == "backend":
            self.inv["all"]["children"][group_frontend]["vars"]["backend_hosts"][name] = {
                "public_ip": ip,
                "private_ip": internal_ip
            }
        elif typ == "frontend":
            self.inv["all"]["children"][group_backend]["vars"]["internal_frontend_ip"] = internal_ip
            self.inv["all"]["children"][group_tunnel]["vars"]["internal_frontend_ip"] = internal_ip
        elif typ == "tunnel":
            self.inv["all"]["children"][group_frontend]["vars"]["tunnel_hosts"][name] = {
                "public_ip": ip,
                "private_ip": internal_ip
            }
        else:
            raise ValueError('Unknown VM type: {}'. format(typ))

    def addInvChild(self, server):
        """Add server to the proper Ansible inventory groups"""

        env = server["metadata"]["env"]
        typ = server["metadata"]["type"]
        if typ == "backend" or typ == "frontend" or typ == "tunnel":
            print("Adding {}".format(typ))
        else:
            print("Unknown type: {}, skipping".format(typ))
            return

        ip = server["public_v4"]
        internal_ip = server["private_v4"]
        purpose = server["metadata"]["purpose"]
        name = server["name"]

        print("IP: {}, internal IP: {}, env: {}, purpose: {}, name: {}".format(ip, internal_ip, env, purpose, name))

        self.checkInvGroup(server)
        group = self.getServerInvGroup(env, typ)

        self.inv["all"]["children"][group]["hosts"][ip] = None
        self.inv["all"]["children"][purpose]["hosts"][ip] = None
        self.inv["all"]["children"][env]["hosts"][ip] = None
        self.inv["all"]["hosts"][ip] = {
            "internal_ip": internal_ip,
            "env": env,
            "type": typ,
            "vmname": name
        }
        self.fillServerInvVars(server)

    def updateInventory(self, outputFilename = 'inventory.json'):
        """Generate the Ansible inventory JSON file from the Openstack instances list"""

        self.clearInventory()

        servers = self.getServersWithMetaKey("env")
        for server in servers:
            if server["public_v4"]:
                self.addInvChild(server)

        with open(outputFilename, 'w') as f:
            json.dump(self.inv, f, indent=4)


