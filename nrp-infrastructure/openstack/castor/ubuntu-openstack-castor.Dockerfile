FROM ubuntu:20.04

ARG CSCS_USER
ARG CSCS_PASS

SHELL ["/bin/bash", "-c"] 

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y sudo git curl wget vim python3-pip less python3-pysaml2 libssl-dev iputils-ping git python-is-python3

RUN pip install -U pip setuptools

RUN pip install -U python-openstackclient lxml oauthlib python-swiftclient python-heatclient

WORKDIR /root/

RUN mkdir -p /etc/openstack && chmod 777 /etc/openstack

COPY cli/castor-cli-otp.env /root/

#ENTRYPOINT source castor-cli-otp.env
