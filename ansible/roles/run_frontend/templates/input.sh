#!/bin/sh

echo TUNNEL_HOST $TUNNEL_HOST
echo TARGET_PORT $TARGET_PORT

# export envs
echo "Overriding SLURM_NTASKS=$SLURM_NTASKS with 1"
export SLURM_NTASKS=1
echo "Overriding SLURM_NPROCS=$SLURM_NPROCS with 1"
export SLURM_NPROCS=1

# checks
pwd
ls -lah
#ls -lah $HOME
ls -lah /scratch/snx3000/${USER}

# create the tunnel
# -TNf option sends the ssh process into background, and $! gives a pid that's not the tunnel
ssh -v -i key-tunneluser -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -R 0.0.0.0:${TARGET_PORT}:localhost:8080 -TN tunneluser@${TUNNEL_HOST} &
tunnelpid=$!
retval=$?
if [ $retval -eq 0 ]; then
    echo "o=====o Tunnel created"
else
    echo "o==/==o Tunnel creation FAILED"
fi

export DAINT_SHARED_DIRECTORY=/scratch/snx3000/$USER

# Using predefined binary of sarus. Needs to looked up using module in the future
# seds are to be replaced programatically in the backend
# greps can be removed once everything works smoothly
/apps/daint/system/opt/sarus/1.0.1/bin/sarus run \
    --mount=source=/etc/opt/slurm/slurm.conf,dst=/usr/local/etc/slurm.conf,type=bind \
    --mount=source=/var/run/munge/munge.socket.2,dst=/var/run/munge/munge.socket.2,type=bind \
    --mount=source=/scratch/snx3000/$USER,dst=/scratch/snx3000/$USER,type=bind \
    --mount=type=bind,source=$HOME,dst=$HOME     hbpneurorobotics/nrp:{{ daint_tag }}     bash -c "\
    unset LD_PRELOAD; rm /usr/lib/libmpi.so.12;  rm /usr/lib/libmpi.so.12.0.2; ln -s /usr/lib/libmpi.so.12.0.5 /usr/lib/libmpi.so.12; \
    export LD_LIBRARY_PATH=/usr/lib:$LD_LIBRARY_PATH; \
    \
    export LC_ALL=C;     unset LANGUAGE ; \
    source /home2/bbpnrsoa/nrp/src/user-scripts/nrp_variables; \
    \
    sed -i 's|<STORAGE_ADDR>|{{ inventory_hostname }}|' /home2/bbpnrsoa/nrp/src/ExDBackend/hbp_nrp_commons/hbp_nrp_commons/workspace/Settings.py; \
    sed -i 's|= MPILauncher|= DaintLauncher|' /home2/bbpnrsoa/nrp/src/BrainSimulation/hbp_nrp_distributed_nest/hbp_nrp_distributed_nest/launch/NestLauncher.py; \
    sed -i 's|sys.executable|\\\"/home2/bbpnrsoa/.opt/platform_venv/bin/python\\\"|' /home2/bbpnrsoa/nrp/src/BrainSimulation/hbp_nrp_distributed_nest/hbp_nrp_distributed_nest/launch/NestLauncher.py; \
    grep -n 'URI' /home2/bbpnrsoa/nrp/src/ExDBackend/hbp_nrp_commons/hbp_nrp_commons/workspace/Settings.py; \
    grep -n 'socket' /home2/bbpnrsoa/.local/etc/nginx/conf.d/nrp-services.conf; \
    grep -n 'auth' /home2/bbpnrsoa/.local/etc/nginx/conf.d/nrp-services.conf; \
    grep -n 'socket' /home2/bbpnrsoa/.local/etc/nginx/uwsgi-nrp.ini; \
    grep -n 'DaintLauncher' /home2/bbpnrsoa/nrp/src/BrainSimulation/hbp_nrp_distributed_nest/hbp_nrp_distributed_nest/launch/NestLauncher.py; \
    which srun; \
    \
    /etc/init.d/supervisor start; \
    sleep 1800; \
    ps uxa | grep python; \
    netstat -tulpen | grep 8080; \
    cat /var/log/supervisor/supervisord.log; \
    cat /var/log/supervisor/nrp-services_app/**; \
    cat /var/log/supervisor/ros-simulation-factory_app/**;"

superpid=$!
retval=$?
if [ $retval -eq 0 ]; then
    echo "Supervisor launched"
else
    echo "Supervisor launch FAILED"
fi

# free resources
kill -9 ${tunnelpid}
kill -9 ${superpid}
