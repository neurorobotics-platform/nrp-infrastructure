---
- name: Copy over container file {{ docker_compose_filename }}
  copy:
    src: '{{ docker_compose_filename }}'
    dest: '/opt/{{ docker_compose_filename }}'
  become: true

# Start the docker backend
- name: docker start backend
  community.docker.docker_compose:
    project_name: opt
    project_src: /opt
    files:
      - '{{ docker_compose_filename }}'
  register: spawned_container
  become: true
  environment:
    DOCKER_REG: "{{ docker_reg }}"
    DOCKER_TAG: "{{ docker_tag }}"
    
# only do these if a new container was spawned
# We need to add this line into /etc/hosts for mvapich to be able to get the hostname of the machine
- name: Run docker-compose etchosts {{ container_name }}
  shell: /usr/bin/docker exec {{ container_name }} bash -c 'echo "127.0.0.1 $(uname -n)" | sudo tee --append /etc/hosts'
  when: spawned_container.changed or force_config

# the variable frontend_group is set inside the host file for each backend group
- name: Update backend config to point to proxy inside the frontend machine {{ container_name }}
  shell: /usr/bin/docker exec {{ container_name }} bash -c "sed -e 's/self\.storage_uri = '\''*.*'\''/self\.storage_uri = '\''http:\/\/{{ internal_frontend_ip }}\/storage'\''/' -i /home/bbpnrsoa/nrp/src/ExDBackend/hbp_nrp_commons/hbp_nrp_commons/workspace/Settings.py"
  when:
    - spawned_container.changed or force_config
    - domain_env == "nodomain"

# the variable frontend_group is set inside the host file for each backend group
- name: Update backend config to point to proxy for {{ container_name }} [Online server version]
  shell: /usr/bin/docker exec {{ container_name }} bash -c "sed -e 's/self\.storage_uri = '\''*.*'\''/self\.storage_uri = '\''https:\/\/{{ domain_env[8:] | replace('.','\.') }}\/storage'\''/' -i /home/bbpnrsoa/nrp/src/ExDBackend/hbp_nrp_commons/hbp_nrp_commons/workspace/Settings.py"
  when:
    - spawned_container.changed or force_config
    - domain_env != "nodomain"

- name: Run docker exec to set to use cpu {{ container_name }}
  shell: /usr/bin/docker exec {{ container_name }} bash -c '/home/bbpnrsoa/nrp/src/user-scripts/rendering_mode cpu'
  when: spawned_container.changed or force_config

- name: Change to running mode {{ running_mode }}  {{ container_name }}
  shell: /usr/bin/docker exec {{ container_name }} bash -c 'sudo chown -R bbpnrsoa:bbp-ext /home/bbpnrsoa/.local/etc/nginx && /home/bbpnrsoa/nrp/src/user-scripts/running_mode {{ running_mode }} no_restart'
  when: spawned_container.changed or force_config

- name: Copy over nginx backend conf {{ container_name }}
  template:
    src: nrp-services.conf
    dest: /tmp/nrp-services.conf
  when: spawned_container.changed and running_mode == 4 or force_config

- name: Copy over lua files {{ container_name }}
  copy:
    src: lua
    dest: /tmp/
  when: spawned_container.changed and running_mode == 4 or force_config

- name: Copy over nginx config and lua into container {{ container_name }}
  shell: |
    /usr/bin/docker cp /tmp/nrp-services.conf {{ container_name }}:/home/bbpnrsoa/.local/etc/nginx/conf.d/
    /usr/bin/docker cp /tmp/lua {{ container_name }}:/home/bbpnrsoa/.local/etc/nginx/
  when: spawned_container.changed and running_mode == 4 or force_config

- name: Update lua with correct variables {{ container_name }}
  shell: |
    /usr/bin/docker exec {{ container_name }} bash -c "sed -e 's|<HBP>/Experiments|/home/bbpnrsoa/.opt/Experiments|' -i /home/bbpnrsoa/.local/etc/nginx/conf.d/nrp-services.conf"
    /usr/bin/docker exec {{ container_name }} bash -c "sed -e 's|<HBP>|/home/bbpnrsoa/nrp/src|' -i /home/bbpnrsoa/.local/etc/nginx/conf.d/nrp-services.conf"
    /usr/bin/docker exec {{ container_name }} bash -c "sed -e 's/<username>/bbpnrsoa/' -i /home/bbpnrsoa/.local/etc/nginx/conf.d/nrp-services.conf"
    /usr/bin/docker exec {{ container_name }} bash -c "sed -e 's|user bbpnrsoa|user root|' -i /home/bbpnrsoa/.local/etc/nginx/nginx.conf"
    /usr/bin/docker exec {{ container_name }} bash -c "sudo chown -R root:root /home/bbpnrsoa/.local/etc/nginx"
    /usr/bin/docker exec {{ container_name }} bash -c "sudo chmod -R og-rwx /home/bbpnrsoa/.local/etc/nginx/*"
    /usr/bin/docker exec {{ container_name }} bash -c "sudo chmod og+r /home/bbpnrsoa/.local/etc/nginx/uwsgi-nrp.ini"
  when: spawned_container.changed or force_config

- name: Update nrp_variables Models var {{ container_name }}
  shell: /usr/bin/docker exec {{ container_name }} bash -c '/bin/sed -e '\''s=$HBP/Models=$HOME/.opt/Models='\'' -i /home/bbpnrsoa/nrp/src/user-scripts/nrp_variables'
  when: spawned_container.changed or force_config

- name: Update nrp_variables Experiments var {{ container_name }}
  shell: /usr/bin/docker exec {{ container_name }} bash -c '/bin/sed -e '\''s=$HBP/Experiments=$HOME/.opt/Experiments='\'' -i /home/bbpnrsoa/nrp/src/user-scripts/nrp_variables'
  when: spawned_container.changed or force_config

- name: List LOW* models
  find:
    paths: 
    - "{{ models_location }}"
    patterns: 'LOW*'
    recurse: yes
  register: find_results
  changed_when: "find_results.matched == 0"
  when: spawned_container.changed

- name: Run create symlinks {{ container_name }}
  shell: /usr/bin/docker exec {{ container_name }} bash -c "source /home/bbpnrsoa/nrp/src/user-scripts/nrp_variables && mkdir -p ~/.gazebo/models && . /home/bbpnrsoa/.opt/Models/create-symlinks.sh"
  when: spawned_container.changed or force_config


- name: Generate schemas {{ container_name }}
  shell: /usr/bin/docker exec {{ container_name }} bash -c 'cd $HOME/nrp/src && source $HOME/.opt/platform_venv/bin/activate && pyxbgen -u $HOME/.opt/Experiments/bibi_configuration.xsd -m bibi_api_gen && pyxbgen -u $HOME/.opt/Experiments/ExDConfFile.xsd -m exp_conf_api_gen && pyxbgen -u $HOME/.opt/Models/environment_model_configuration.xsd -m environment_conf_api_gen && pyxbgen -u $HOME/.opt/Models/robot_model_configuration.xsd -m robot_conf_api_gen && deactivate'
  when: spawned_container.changed or force_config

- name: Move schemas over {{ container_name }}
  shell: /usr/bin/docker exec {{ container_name }} bash -c 'gen_file_path=$HBP/ExDBackend/hbp_nrp_commons/hbp_nrp_commons/generated && filepaths=$HOME/nrp/src && sudo cp $filepaths/bibi_api_gen.py $gen_file_path &&  sudo cp $filepaths/exp_conf_api_gen.py $gen_file_path && sudo cp $filepaths/robot_conf_api_gen.py $gen_file_path && sudo cp $filepaths/environment_conf_api_gen.py $gen_file_path'
  when: spawned_container.changed or force_config

- name: Enable run-experiments.ini for demo server
  shell: /usr/bin/docker exec {{ container_name }} bash -c 'sudo cp /etc/supervisord.d/run-experiments.ini_ /etc/supervisord.d/run-experiments.ini'
  ignore_errors: true
  when: spawned_container.changed and 'demo_backends' in group_names

- name: Stop supervisor {{ container_name }}
  become: true
  ignore_errors: true
  shell: /usr/bin/docker exec {{ container_name }} bash -c "sudo /etc/init.d/supervisor stop && sleep 10"
  when: spawned_container.changed or force_config

- name: Start supervisor {{ container_name }}
  become: true
  shell: /usr/bin/docker exec {{ container_name }} bash -c "sudo /etc/init.d/supervisor start && sleep 5 && killall uwsgi"
  when: spawned_container.changed or force_config

