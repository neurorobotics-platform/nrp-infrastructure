local http = require("socket.http")
local ltn12 = require "ltn12"
local json = require("cjson")


local cache = {}
local cache_ttl = 300 -- Time to live for cache entries in seconds

function url_encode(str)
    if (str) then
        str = string.gsub (str, "\n", "\r\n")
        str = string.gsub (str, "([^%w ])",
            function (c) return string.format ("%%%02X", string.byte(c)) end)
        str = string.gsub (str, " ", "+")
    end
    return str   
end

function introspect_token(txn)
    txn:set_var('txn.unauthorized', 'The request is unauthorized')

    local headers = txn.http:req_get_headers()
    local auth_header = headers['authorization'][0]
    -- txn:Debug(auth_header)
    if not auth_header then
        txn:done({status  = 401, reason  = "Missing Authorization header", body = "Missing Authorization header\n"})
    end
    
    local token = string.match(auth_header, "^Bearer%s+(.+)")
    -- txn:Debug('token: ' .. token)
    if not token then
        txn:done({status  = 401, reason  = "Invalid Authorization header format", body = "Invalid Authorization header format\n"})
    end
    
    local cached_entry = cache[token]
    if cached_entry and os.time() < cached_entry.expiry then
        if not cached_entry.is_active then
            txn:done({status  = 401, reason  = "Invalid or expired token", body = "Invalid or expired token\n"})
        end
        txn:unset_var('txn.unauthorized')
        return
    end
    
    local client_id = "{{ oidc_proxy_client_id }}"
    local client_secret = "{{ oidc_proxy_secret }}"
    local introspection_endpoint = "{{ oidc_realm }}realms/hbp/protocol/openid-connect/token/introspect"
    local payload = "client_id="..url_encode(client_id).."&client_secret="..url_encode(client_secret).."&token="..url_encode(token)
    
    -- local r, msg = http.request("POST", introspection_endpoint, payload)
    local respbody = {}
    txn:Info('Starting token introspection...')
    local  body, code, headers, status = http.request{
        method = "POST",
        url = introspection_endpoint,
        headers = {
            ["Content-Type"] = "application/x-www-form-urlencoded",
            ['Content-Length'] = string.len(payload)
        },
        source = ltn12.source.string(payload),
        sink = ltn12.sink.table(respbody)
    }
    -- txn:Debug('body:' .. tostring(body))
    -- txn:Debug('code:' .. tostring(code))
    -- txn:Debug('headers:' .. table.concat(headers))
    -- txn:Debug('status:' .. tostring(status))
    -- txn:Debug('respbody:' .. table.concat(respbody))

    if code == 200 then
        local result = json.decode(table.concat(respbody))

        cache[token] = {
            is_active = result.active,
            expiry = result.exp
        }
        txn:Info('Token saved in cache')
    
        if result.active ~= true then
            txn:done({status  = 401, reason  = "Invalid or expired token", body = "Invalid or expired token\n"})
        end

        txn:unset_var('txn.unauthorized')
    end
end

core.register_action("introspect_token", {"http-req"}, introspect_token, 0)
